{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverlappingInstances #-}
module Sol4 where 

import SetOrd

import Data.Char
import Data.List
import Test.Hspec
import Test.QuickCheck
import Control.Monad
import System.Random

------------------------------------------- UTIL ------------------------------------------- 

fp :: Eq a => (a -> a) -> a -> a
fp f = \ x -> if x == f x then x else fp f (f x)

infix 1 ===>

(===>) :: Bool -> Bool -> Bool
x ===> y = (not x) || y

deepCheck p = quickCheckWith (stdArgs{ maxSuccess = 10000 }) p

getRandomInt :: Int -> IO Int
getRandomInt n = getStdRandom (randomR (0,n))

------------------------------------------- Exercise 2 ------------------------------------------- 

-- Boryana : ...
-- Nikola : About 5 hours, together with reading up on functors etc.
-- Matthisk : Time spent, about 4 hours (including research into Functors, Applicative Functors and Monads)

------------------------------------------- Exercise 3 ------------------------------------------- 
-- Manually
randomSets :: Int -> Int -> IO([Set Int])
randomSets 0 _ = return []
randomSets n l = do 
	x <- randomSet l
	y <- randomSets (n-1) l
	return $ x : y

randomSet :: Int -> IO (Set Int)
randomSet n = do
	gen <- newStdGen 
	return $ list2set $ rndIntList n gen

rndIntList :: Int -> StdGen -> [Int]
rndIntList n gen = take n $ nub $ randomRs (1,2 * n) gen

instance Arbitrary (Set Int) where
    arbitrary = sized $ \n -> do 
        k <- choose (0,n)
        l <- sequence [ choose(0,2 * k) | _ <- [1..k] ] 
        return $ list2set l

sample_sets :: IO ()
sample_sets = sample $ (arbitrary :: Gen (Set Int))

no_dups :: (Eq a) => [a] -> Bool
no_dups [] = True
no_dups (x:xs) = (not $ elem x xs) && no_dups xs

ordered :: (Ord a) => [a] -> Bool
ordered []  = True
ordered [x] = True
ordered (x:y:xs) = x < y && ordered (y:xs)

prop_dups :: (Arbitrary a, Eq a) => Set a -> Bool
prop_dups (Set xs) = no_dups xs

prop_ord :: (Arbitrary a, Ord a) => Set a -> Bool
prop_ord (Set xs) = ordered xs

------------------------------------------- Exercise 4 ------------------------------------------- 
-- Implementation
-- Time Spent : 2 hours
intersectionSet :: (Eq a) => Set a -> Set a -> Set a
intersectionSet (Set a) (Set b) = Set $ [ x | x <- a, elem x b ]

differenceSet :: (Eq a) => Set a -> Set a -> Set a
differenceSet (Set a) (Set b) = Set $ [ x | x <- a, not $ elem x b ]

-- Union set is defined in SetOrd and for fun here is the implementation of the symmetric difference:
symDifferenceSet :: (Eq a, Ord a) => Set a -> Set a -> Set a
symDifferenceSet s1 s2 = (differenceSet s1 s2) `unionSet` (differenceSet s2 s1)

-- tests

-- A ^ B : { x | x < A and x < B }
prop_intersection :: (Arbitrary a, Eq a, Ord a) => Set a -> Set a -> Bool
prop_intersection (Set a) (Set b) = all (\x -> x `elem` b ===> x `elem` c) a
    where (Set c) = intersectionSet (Set a) (Set b)

-- A - B : { x | x < A and x !< B }
prop_difference :: (Arbitrary a, Eq a, Ord a) => Set a -> Set a -> Bool
prop_difference (Set a) (Set b) = all (\ x -> (not $ x `elem` b) ===> x `elem` c) a
    where (Set c) = differenceSet (Set a) (Set b)

-- A u B : { x | x < A or x < B}
prop_union :: (Arbitrary a, Eq a, Ord a) => Set a -> Set a -> Bool
prop_union (Set a) (Set b) = all (`elem` c) a && all (`elem` c) b
    where (Set c) = unionSet (Set a) (Set b)

-- A / B : { x | x < A and x !< b or x < B and x !< A }
prop_sym_difference :: Ord a => Set a -> Set a -> Bool
prop_sym_difference (Set a) (Set b) = all symDiff a && all symDiff b where 
	(Set c) = symDifferenceSet (Set a) (Set b)
	symDiff = \ x -> x `elem` c || (x `elem` a && x `elem` b)

-- automated random testing
test :: (Show a, Show b) => Int -> (a -> b -> Bool) -> [a] -> [b] -> IO ()
test n _ [] [] = print (show n ++ " tests passed")
test n p (f:fs) (g:gs) =
  if p f g
  then do print ("pass on:" ++ show f ++ " and " ++ show g)
          test n p fs gs
  else error ("failed test on:" ++ show f ++ " and " ++ show g)

testIntersect :: Int -> Int -> IO ()
testIntersect amnt len = do
	x <- randomSets amnt len
	y <- randomSets amnt len
	test amnt prop_intersection x y

testUnion :: Int -> Int -> IO ()
testUnion amnt len = do
	x <- randomSets amnt len
	y <- randomSets amnt len
	test amnt prop_union x y

testDifference :: Int -> Int -> IO ()
testDifference amnt len = do
	x <- randomSets amnt len
	y <- randomSets amnt len
	test amnt prop_difference x y

testSymDifference :: Int -> Int -> IO ()
testSymDifference amnt len = do
	x <- randomSets amnt len
	y <- randomSets amnt len
	test amnt prop_sym_difference x y

-- Testing using QuickCheck
check_intersection :: IO ()
check_intersection = verboseCheck (prop_intersection :: (Set Int) -> (Set Int) -> Bool)

check_difference :: IO ()
check_difference = verboseCheck (prop_difference :: (Set Int) -> (Set Int) -> Bool)

check_union :: IO ()
check_union = verboseCheck (prop_union :: (Set Int) -> (Set Int) -> Bool)

check_sym_difference :: IO ()
check_sym_difference = verboseCheck (prop_sym_difference :: (Set Int) -> (Set Int) -> Bool)

------------------------------------------- Exercise 5 ------------------------------------------- 
type Rel a = [(a,a)]

infixr 5 @@

(@@) :: Eq a => Rel a -> Rel a -> Rel a
r @@ s = nub [ (x,z) | (x,y) <- r, (w,z) <- s, y == w ]

(+++) :: Eq a => Rel a -> Rel a -> Rel a
[] +++ s = s
(r:rs) +++ s | r `elem` s = rs +++ s
             | otherwise  = r : (rs +++ s)

trClos :: Ord a => Rel a -> Rel a
trClos x | x == y = x | otherwise = trClos y where y = (x @@ x) +++ x

------------------------------------------- Exercise 6 ------------------------------------------- 
isTransitive :: Eq a => Rel a -> Bool
isTransitive r = and $ [ False | (x,y) <- r, (w,z) <- r, y == w && (not $ (x,z) `elem` r) ]

-- removing any element from trClos R that does not belong to R makes the trClos R non-transitive
minimal :: (Ord a, Eq a) => Rel a -> Bool
minimal r = and [ not $ isTransitive (delete x r) | x <- trClos r, not $ elem x r]

rel2Els :: (Eq a, Ord a) => [(a,a)] -> [a]
rel2Els = sort . nub . foldr (\(x,y) z-> x : y : z) []

prop_transitive :: (Arbitrary a, Eq a, Ord a) => Rel a -> Bool
prop_transitive = isTransitive . trClos

trClosSpec :: IO ()
trClosSpec = hspec $ do
    describe "trlClos" $ do
        it "contains the input relation R" $ do
            property $ \ r -> trClos (r :: Rel Int) `shouldContain` (r :: Rel Int)

        it "is a transitive relation" $ do
            property $ \ r -> isTransitive $ trClos (r :: Rel Int)

        it "only contains the same elements" $ do
            property $ \ r -> (rel2Els $ trClos (r :: Rel Int)) `shouldBe` rel2Els (r :: Rel Int)

        it "is mininal" $ do
            property $ \ r -> minimal $ trClos (r :: Rel Int)

testTrClos :: IO ()
testTrClos = hspec $ do 
	describe "trClos" $ do 
		it "Transitive closure of an empty relation R is the same empty relation R" $ do
			((trClos[] :: [(Int, Int)]) `shouldBe` ([] :: [(Int, Int)]))
		it "Transitive closure of a transitive relation R is the same relation R" $ do
			((trClos[(1,2), (2,1), (1,1), (2,2)] :: [(Int, Int)]) `shouldBe` (trClos[(1,2), (2,1), (1,1), (2,2)] :: [(Int, Int)]))
		it "Transitive closure of a non-transitive relation R is the smallest transitive relation P which contains R" $ do
			((sort $ trClos [(1,2), (2,3), (3,2), (3,4), (4,5)] :: [(Int, Int)]) `shouldBe` (sort $ trClos [(1,2), (2,3), (3,2), (3,4), (4,5), (1,3), (2,2), (2,4), (3,3), (3,5), (1,4), (2,5), (1,5)] :: [(Int, Int)]))


------------------------------------------- Exercise 7 ------------------------------------------- 

instance Arbitrary (Rel Int) where
	arbitrary = sized $ \ n -> let n2 = n `div` 2 in randomRel n2 (n2 * (n2-1) `div` 2)

-- Generate a random relation:

randomRel :: Int -> Int -> Gen (Rel Int)
randomRel 0 _ = return []
randomRel n s
	| 1 + n * (n-1) < s = error ("A graph with " ++ show n ++ " nodes can not have " ++ show s ++ " edges.")
	| otherwise = do
	    let nodes@(x:xs) = [1..n]
	    g <- nodes2Graph [x] xs
	    f <- addEdges (s - (n - 1)) nodes g
	    return f

getRandomTuple :: Int -> Gen (Int,Int)
getRandomTuple n = do
    r1 <- choose( 0, n )
    r2 <- choose( 0, n )
    return (r1,r2)

addEdges :: Int -> [Int] -> Rel Int -> Gen (Rel Int)
addEdges s nodes rel
    | s > 0 = do
        (a,b) <- getRandomTuple (length nodes - 1)
        let n1 = nodes !! a
        let n2 = nodes !! b
        if n1 == n2 || elem (n1,n2) rel
            then addEdges s nodes rel
            else addEdges (s-1) nodes ( (n1,n2) : rel )
    | otherwise = return rel 

nodes2Graph :: (Eq a, Ord a) => [a] -> [a] -> Gen (Rel a)
nodes2Graph _ [] = return $ []
nodes2Graph l (x:ys) = do
    r <- choose $ ( 0, (length l - 1) )
    rest <- nodes2Graph (x:l) (ys)
    return $ (l !! r,x) : rest

