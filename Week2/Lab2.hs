module Lab2 where 

import Data.List
import System.Random

--Exercise 1
data Shape = NoTriangle | Equilateral 
			| Isosceles | Rectangular | Other deriving (Eq,Show)

triangle :: Integer -> Integer -> Integer -> Shape
triangle x y z 
 | orderIndependent (\a b c -> a + b <= c) x y z = NoTriangle 
 | x == y && y == z = Equilateral 
 | orderIndependent (\a b c -> a ^ 2 + b ^ 2 == c ^ 2) x y z = Rectangular
 | x == y || y == z || z == x = Isosceles
 | otherwise = Other

orderIndependent :: (a -> a -> a -> Bool) -> a -> a -> a -> Bool
orderIndependent f x y z = f x y z || f x z y || f y z x

-- Exercise 2
isPermutation :: Eq a => [a] -> [a] -> Bool
isPermutation [] [] = True
isPermutation _  [] = False
isPermutation [] _  = False
isPermutation (x:xs) ys = isPermutation xs $ delete x ys

--Exercise 3
--If A and B are the two arguments passed to the isPermutation and 
--there is no duplication of elements in set A, then there should be a 
--bijection between the two sets. In this sense, testable properties are
--the cardinality of the two sets and the specific elements they contain
isPermutationTestb = not $ isPermutation [1] []
isPermutationTestc = not $ isPermutation [] [1]
isPermutationTestd = id $ isPermutation [1] [1]
isPermutationTeste = not $ isPermutation [1] [2]
isPermutationTestf = not $ isPermutation [1] [1, 2]
isPermutationTestg = not $ isPermutation [1, 2] [1]
isPermutationTesti = id $ isPermutation [1, 2] [2, 1]
isPermutationTestj = not $ isPermutation [1, 2] [2, 3]
isPermutationTestl = id $ isPermutation [1,2,3,4] [1,2,3,4]
isPermutationTestm = id $ isPermutation "software" "waresoft"
isPermutationTestn = not $ isPermutation "software" "waresoft1"

--Exercise 4
perms :: Eq a => [a] -> [[a]]
perms [] = [[]]
perms xs = [ i:j | i <- xs, j <- perms $ delete i xs ]

testPerms :: Eq a => [a] -> Bool
testPerms xs = all (isPermutation xs) (perms xs)

factorial :: Int -> Int
factorial 0 = 1
factorial x = x * factorial (x-1)

testPermsLength :: Eq a => [a] -> Bool
testPermsLength xs = factorial (length xs) == length (perms xs)

--Exercise 5
isDerangement :: Eq a => [a] -> [a] -> Bool
isDerangement xs ys 
 | not $ isPermutation xs ys = False
 | otherwise = and $ zipWith (/=) xs ys

--Exercise 6
deran :: Int -> [[Int]]
deran n = filter (isDerangement l) (perms l) where l = [0..(n-1)] 

--Exercise 7
--Similarly to the isPermutation tests, the isDerangement should have test
--concerning the lenght of the two arguments and set elements. Additionally,
--no element of A should appear on the same position in B.
--isDerangementTestA = isDerangement [] [] 
isDerangementTesta = not $ isDerangement [1] []
isDerangementTestb = not $ isDerangement [] [1]
isDerangementTestc = not $ isDerangement [1] [1]
isDerangementTestd = not $ isDerangement [1] [2]
isDerangementTeste = not $ isDerangement [1] [1, 2]
isDerangementTestf = not $ isDerangement [1, 2] [1]
isDerangementTestg = not $ isDerangement [1, 2] [1, 2]
isDerangementTesth = id $ isDerangement [1, 2] [2, 1]
isDerangementTesti = not $ isDerangement [1, 2] [2, 3]
isDerangementTestj = id $ isDerangement "brown" "nbrow"
isDerangementTestk = not $ isDerangement "brown" "nbrww"

--Exercise 8
--The function randomDeran picks a random element from the set of
--all derangements of [0..n]. All test of the isDerangement function
--will apply to the output of the randomDeran
getRandomInt :: Int -> IO Int
getRandomInt n = getStdRandom (randomR (0,n))

randomDeran :: Int -> IO ()
randomDeran n = do
	let dPerms = deran n
	r <- getRandomInt $ length dPerms - 1
	print $ dPerms !! r

--Exercise 9
numDeran :: Integer -> Integer
numDeran 0 = 1
numDeran 1 = 0
numDeran n = (n-1) * (numDeran (n-1) + numDeran (n-2))
