module Lab5

where
import Data.List
import SS
import Test.Hspec
import Test.QuickCheck
import System.Random

-- Ex 1
sudokuSpec :: Sudoku -> IO ()
sudokuSpec s = hspec $ do
	describe "Sudoku" $ do
		it "should always be consistent" $ do
			consistent s
		it "should always have unique solution" $ do
			uniqueSol (s, constraints s)

randomSudokuSpec :: IO ()
randomSudokuSpec = do
	n <- genRandomSudoku
	showNode n
	sudokuSpec $ fst n

-- Ex 2

prop_minimal :: Sudoku -> Bool
prop_minimal s = uniqueSol (s, constraints s) && minimal' s where
	minimal' :: Sudoku -> Bool
	minimal' s = all (\ s -> not $ uniqueSol (s, constraints s)) [ extend s ((r,c),0) | r <- positions, c <- positions, s (r,c) /= 0 ]

randomProblemMinimal :: IO (Bool)
randomProblemMinimal = do
	n <- genRandomSudoku
	showNode n
	(s,c) <- genProblem n
	showNode (s,c)
	return $ prop_minimal $ s

-- Ex 3
-- http://puzzling.stackexchange.com/questions/309/what-is-the-maximum-number-of-empty-3x3-blocks-a-proper-sudoku-can-have
extendBlock :: Sudoku -> ((Row,Column),Value) -> Sudoku
extendBlock s ((rb,cb),v) = foldl (extend) s [ ((r,c),v) | r <- blocks !! rb, c <- blocks !! cb ]

extendBlocks :: Sudoku -> [(Row,Column)] -> Value -> Sudoku
extendBlocks s bs v = foldl (extendBlock) s $ map (\t -> (t,v)) bs

combinations :: Int -> [a] -> [[a]]
combinations k ns = filter ((k==).length) (subsequences ns)

getAllBlockCombinations :: Int -> [[(Row,Column)]]
getAllBlockCombinations n = combinations n [ (r,c) | r <- [0..(length blocks - 1)], c <- [0..(length blocks - 1)] ]

genAllBlockCombinations :: Int -> IO [[(Row,Column)]]
genAllBlockCombinations n = randomize $ getAllBlockCombinations n

getCustomSudoku :: Sudoku -> [[(Row,Column)]] -> [Sudoku]
getCustomSudoku _ [] = []
getCustomSudoku s (b:bs) = let s' = extendBlocks s b 0 in if uniqueSol (s', constraints s') then return s' else getCustomSudoku s bs

genCustomProblem :: Int -> IO ()
genCustomProblem n = do 
	[(s,_)] <- rsolveNs [emptyN]
	bs <- genAllBlockCombinations n
	let s' = getCustomSudoku s bs
	if null s' 
		then print "No problem exists" 
		else do
			let node = (head $ s', constraints $ head s')
			showNode node
			problem  <- genProblem node
			showNode problem

-- Ex 6

isSimpleSudoku :: Node -> Bool
isSimpleSudoku n@(s,cs) 
	| null cs = True
	| null sols = False
	| otherwise = let s' = foldl extend s sols in isSimpleSudoku (s', constraints s')
	where sols = findSimpleSolutions n

findSimpleSolutions n = findHiddenSingles n ++ findNakedSingles n

findHiddenSingles :: Node -> [((Row,Column),Value)]
findHiddenSingles (s,constraints) = concat $ findHiddenSingles' constraints where
	findHiddenSingles' [] = []
	findHiddenSingles' ((r,c,vs):cs) = ((hiddenSingles (r,c,vs) $ concat [ vs2 | (r2,c2,vs2) <- constraints, r == r2]) `union` 
										(hiddenSingles (r,c,vs) $ concat [ vs2 | (r2,c2,vs2) <- constraints, c == c2]) `union` 
										(hiddenSingles (r,c,vs) $ concat [ vs2 | (r2,c2,vs2) <- constraints, elem r2 (bl r) && elem c2 (bl c)])) 
										: 
										findHiddenSingles' cs

hiddenSingles :: (Row,Column,[Value]) -> [Value] -> [((Row,Column),Value)]
hiddenSingles (r,c,vs) values = [ ((r,c),v) | v <- vs, notElem v $ delete v values ]

findNakedSingles :: Node -> [((Row,Column),Value)]
findNakedSingles (s,[]) = []
findNakedSingles (s, ((r,c,vs):cs) )
		| length vs == 1 = ((r,c),head vs) : findNakedSingles (s,cs)
		| otherwise = findNakedSingles (s,cs)

simplify :: Node -> [(Row,Column)] -> Node
simplify n [] = n
simplify n ((r,c):rcs) 
   | isSimpleSudoku n' = simplify n' rcs
   | otherwise    = simplify n  rcs
  where n' = eraseN n (r,c)

genSimpleProblem :: Node -> IO Node
genSimpleProblem n = do 
	ys <- randomize xs
	return (simplify n ys)
      where xs = filledPositions (fst n)

generateEasySudoku :: IO Node
generateEasySudoku = do 
	[r] <- rsolveNs [emptyN]
	showNode r
	s  <- genProblem r
	showNode s
	return s

generateDifficultSudoku :: IO Node
generateDifficultSudoku = do 
	s <- genRandomSudoku
	p <- genProblem s
	if not $ isSimpleSudoku p then do showNode p; return p
	else generateDifficultSudoku

-- Refutation sum score from the paper:
refRating :: Node -> IO (Int)
refRating n = do 
	scores <- sequence [ refScore n (r,c) 0 | r <- positions, c <- positions ]
	return $ sum scores

refScore :: Node -> (Row,Column) -> Int -> IO (Int)
refScore n@(s,cs) (r,c) counter
	| s (r,c) /= 0 = return $ counter
	| solvedCell (r,c) cs = return $ counter + 1
	| otherwise = do
		let sols = findSimpleSolutions n
		if null $ sols 
			then return $ (maxBound :: Int)
			else do
				rSols <- randomize sols
				let sol = head rSols
				let n' = (let next = s `extend` sol in (next, constraints next))
				refScore n' (r,c) (counter + 1)

solvedCell :: (Row,Column) -> [Constraint] -> Bool
solvedCell (row,column) cs = not $ null [ (r,c,vs) | (r,c,vs) <- cs, r == row && c == column && length vs == 1 ] 

-- Ex 6
