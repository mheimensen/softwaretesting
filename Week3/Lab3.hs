import Week3

-- Test Util

genTest :: (Show a, Show b) => Int -> (a -> b -> Bool) -> [a] -> [b] -> IO ()
genTest n _ [] [] = print (show n ++ " tests passed")
genTest n p (f:fs) (g:gs) = 
  if p f g 
  then do print ("pass on:" ++ show f ++ " and " ++ show g)
          genTest n p fs gs
  else error ("failed test on:" ++ show f ++ " and " ++ show g)

-- End Test Util

-- Exercise 1
contradiction :: Form -> Bool
contradiction f = all (\v -> not $ eval v f) (allVals f)

tautology :: Form -> Bool
tautology f = all (\v -> eval v f) (allVals f)

-- logical entailment
entails :: Form -> Form -> Bool
entails f g = tautology $ Impl f g

-- logical equivalence
equiv :: Form -> Form -> Bool
equiv f g = tautology $ Equiv f g

-----------
-- TESTS --
-----------

formContradiction = Cnj [p, Neg p]
formTautology = Dsj [p, Neg p]

testRandomFormulasFor :: Int -> Int -> (Form -> Bool) -> IO ()
testRandomFormulasFor d n p = do
	fs <- getRndFs d n
	test n p fs

testRandomFormulasForContradiction :: Int -> Int -> IO ()
testRandomFormulasForContradiction d n = testRandomFormulasFor d n (\f -> contradiction (Cnj [f, Neg f]))

testFormulasForContradiction :: IO ()
testFormulasForContradiction = test (length fs) contradiction fs where
		fs = [formContradiction,
			  Neg formTautology,
			  Neg form1,
			  Neg form3]

testRandomFormulasForTautology :: Int -> Int -> IO ()
testRandomFormulasForTautology d n = testRandomFormulasFor d n (\f -> tautology (Dsj [f, Neg f]))

testFormulasForTautology :: IO ()
testFormulasForTautology = test (length fs) tautology fs where
		fs = [formTautology,
			  Neg formContradiction,
			  form1,
			  form3]

testRandomFormulasForImplication :: Int -> Int -> IO ()
testRandomFormulasForImplication d n = testRandomFormulasFor d n (\f -> entails f f)

testFormulasForImplication :: IO ()
testFormulasForImplication = genTest (length fs) entails fs gs where
		fs = [formTautology, p, Dsj [p, q], Cnj [p, q] ]
		gs = [formTautology, Dsj [p, q], Dsj [p, q], Dsj [p, q]]

testFormulasForEquivalence :: IO ()
testFormulasForEquivalence = genTest (length fs) equiv fs gs where
		fs = [formTautology, formContradiction, p, Dsj [p, q], Cnj [p, p], q]
		gs = [formTautology, formContradiction, Neg (Neg p), Dsj [q, p], p, Dsj [q, q]]

-- End Exercise 1

-- Exercise 2
formulaToCnf :: Form -> Form
formulaToCnf f = flatten $ cnf $ nnf $ arrowfree f

cnf :: Form -> Form
cnf (Prop x) = Prop x
cnf (Neg (Prop x)) = Neg (Prop x)
cnf (Dsj []) = Dsj []
cnf (Cnj fs) = Cnj $ map cnf fs
cnf (Dsj fs) = dist $ map cnf fs

dist :: [Form] -> Form
dist (f:fs) = foldr dist' f fs where
	dist' :: Form -> Form -> Form
	dist' (Cnj xs) f = Cnj $ map (\x -> dist' x f) xs
	dist' f (Cnj xs) = Cnj $ map (\x -> dist' f x) xs
	dist' x y = Dsj [x, y] 

flatten :: Form -> Form
flatten f@(Cnj fs) = Cnj (flattenCnj f)
flatten f@(Dsj fs) = Dsj (flattenDsj f)
flatten f = f

flattenCnj :: Form -> [Form]
flattenCnj (Cnj fs) = foldr (++) [] (map flattenCnj fs)
flattenCnj f = [flatten f] 

flattenDsj :: Form -> [Form]
flattenDsj (Dsj fs) = foldr (++) [] (map flattenDsj fs)
flattenDsj f = [flatten f]
-- End Exercise 2

-- Exercise 3
{- 
Properties tested: 
	1) if the two forms are equivalent
	2) the allowed tree is a conjunction of 2 possible values (mixing allowed):
		a) disjunctions of props (neg props)
		b) props (neg props)
	or any possible subtree of this tree
-}
testConversion :: Form -> Bool
testConversion f = equiv f g && isCnf g
	where g = formulaToCnf f

isCnf :: Form -> Bool
isCnf (Cnj xs) = all dsjStep xs
isCnf x = dsjStep x

dsjStep :: Form -> Bool
dsjStep (Dsj xs) = all propStep xs 
dsjStep x = propStep x

propStep :: Form -> Bool
propStep (Neg (Prop x)) = True
propStep (Prop x) = True
propStep _ = False

-- tests a conversion of a random formula 
testRndConversion = do 
	f <- getRandomF
	return $ testConversion f

testRndConversions n = do 
	f <- getRandomFs n
	test n testConversion f
-- End Exercise 3

-- Exercise 4

type Clause = [Int]
type Clauses = [Clause]

cnf2cls :: Form -> Clauses
cnf2cls (Cnj xs) = map form2clause xs
cnf2cls xs = [form2clause xs]

form2clause :: Form -> Clause
form2clause (Prop x) = [x]
form2clause (Neg (Prop x)) = [-x]
form2clause (Dsj xs) = concat $ map form2clause xs

form2cls :: Form -> Clauses
form2cls f = cnf2cls $ cnf f

-----------
-- TESTS --
-----------

zipEquals :: (a -> b -> Bool) -> [a] -> [b] -> Bool
zipEquals f xs ys = (length xs == length ys) && (and $ zipWith f xs ys)

formIscls :: Form -> Clauses -> Bool
formIscls (Cnj fs) cs = zipEquals dsjEqualsCls fs cs
formIscls (Dsj fs) [cs] = zipEquals propEqualsInt fs cs
formIscls f [[c]] = propEqualsInt f c
formIscls _ _ = False

dsjEqualsCls :: Form -> Clause -> Bool
dsjEqualsCls (Dsj fs) cs = zipEquals propEqualsInt fs cs 
dsjEqualsCls f [c] = propEqualsInt f c
dsjEqualsCls _ _ = False

propEqualsInt :: Form -> Int -> Bool
propEqualsInt (Prop x) p = x == p
propEqualsInt (Neg (Prop x)) p = (-x) == p
propEqualsInt _ _ = False

testCls :: Int -> Int -> IO ()
testCls d n = do
  fs <- getRndFs d n
  let cnfs = map formulaToCnf fs
  genTest n formIscls cnfs (map cnf2cls cnfs)

-- End Exercise 4