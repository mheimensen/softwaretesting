module ModExp (modExp_lr, modExp_rl) where

import Data.Bits
import Data.List

{- For this exercise I tried two algorithms for computing modular exponentiation.
   The first one is the RL method, which is similar to the concepts explained in the slides
   The second method is the (k ary) LR method which is also used in Pythons implementation of
   modular exponentation (named pow). This method is significantly faster because the multiplications
   of very large integers is avoided
-}

{- For this exercise I tried two algorithms for computing modular exponentiation.
   The first one is the RL method, which is similar to the concepts explained in the slides
   The second method is the (k ary) LR method which is also used in Pythons implementation of
   modular exponentation (named pow). This method is significantly faster because the multiplications
   of very large integers is avoided
-}

tableOfExp :: Integer -> Integer -> Int -> [Integer]
tableOfExp a n base = take base $ iterate (\ r -> r * a `mod` n) 1  

digitsOfN :: Integer -> Integer -> [Integer]
digitsOfN n base = reverse $ map (`mod` base) $ takeWhile (> 0) $ iterate (`div` base) n

{- modExp_lr :: Computing the modular exponentation of a^b mod n using the (k ary) LR method
                Running time of this algorithm O( k * klog(b) )
   ref: http://eli.thegreenplace.net/2009/03/28/efficient-modular-exponentiation-algorithms
-}
modExp_lr :: Integer -> Integer -> Integer -> Int ->  Integer
modExp_lr a b n k
    | b < 0 =  error "Negative exponent"
    | otherwise = 
        let base = 2 `shift` (k - 1)
            comps = tableOfExp a n base
            digits = digitsOfN b $ toInteger base
            compute r d = 
                let r' = iterate (\ x -> x * x `mod` n) r !! k
                in if d /= 0 
                    then r' * (comps !! (fromInteger d)) `mod` n 
                    else r'
    	in foldl compute 1 digits

{- modExp_rl :: Computing the modular exponentation of a^b mod n using the RL method
                Running time of this algorithm O( 2log(b) )
-}
modExp_rl :: Integer -> Integer -> Integer -> Integer
modExp_rl = exM' 1 where
    exM' r a b n
        | b < 0 = error "Negative exponent"
        | b == 0 = r 
        | b `mod` 2 == 1 = exM' (r * a `mod` n) (a * a `mod` n) (b `div` 2) n 
        | otherwise = exM' r (a * a `mod` n) (b `div` 2) n
