module Lab6 

where

import Control.Applicative
import Control.DeepSeq
import Control.Exception (evaluate)

import Data.Bits
import Data.List

import System.CPUTime ( getCPUTime )
import System.Random

import Test.QuickCheck
import Test.Hspec
import Text.Printf 

import ModExp
import Week6

-- Ex 1
{- TimeSpent
    Matthisk : 7h
-}

{-
    For implementation of modular exponentation see ModExp.hs
-}

{- To compare the speed of the two algorithms i wrote some tests.
   Test the speed by running speedTest_modExp_rl or speedTest_modExp_lr for the two algorithms
   Supply the function a number on how many random numbers to test on (e.g. 10.000)
-}

listOfLargeNumbers :: Int -> IO [Integer]
listOfLargeNumbers 0 = return []
listOfLargeNumbers n = (:) <$> randomLargeNumber 512 <*> listOfLargeNumbers (n-1)

listOfLargeNumberTuples :: Int -> IO [(Integer,Integer)]
listOfLargeNumberTuples n = listOfLargeNumbers n >>= \xs -> listOfLargeNumbers n >>= \ys -> return $ zip xs ys

speedTest :: (Integer -> Integer -> Integer -> Integer) -> [(Integer,Integer)] -> IO ()
speedTest f xs = do
    m <- randomRIO (0,100)
    start <- getCPUTime
    let r = map (\ (a,b)  -> f a b m) xs
    end <- r `deepseq` getCPUTime
    let diff = (fromIntegral (end - start)) / (10^12)
    printf "Computed exponent modulo in: %0.3f sec\n" (diff :: Double)

speedTest_modExp_rl n = listOfLargeNumberTuples n >>= speedTest modExp_rl
speedTest_modExp_lr n = listOfLargeNumberTuples n >>= speedTest (\ a b n -> modExp_lr a b n 5)
-- Ex 2
{- TimeSpent
    Matthisk : 2h
-}

{- prop_exM :: Checks if the result for our implementation of modular exponentation is equal to the supplied algorithm (expM)
               This property can be tested using quickcheck, run: quickCheck prop_exM
-}
prop_exM :: Integer -> Positive Integer -> NonZero Integer -> Bool
prop_exM a (Positive b) (NonZero n) =
    let exM1 = modExp_rl a b n 
        exM2 = modExp_lr a b n 5
        exM3 = exM a b n
    in (exM1 == exM3) && (exM2 == exM3)

{- hSpec implementation for modular exponentation testing intersting edge cases
-}

test_modExp :: IO ()
test_modExp = hspec $ do 
    describe "modular exponentation" $ do 
        it "throws an exception when trying to supply a negative exponent" $ do
            property $ \ (Positive b) -> evaluate (modExp_lr 9 (-b) 2 5) `shouldThrow` errorCall "Negative exponent"
        it "throws an exception when trying to use 0 as a modulo" $ do
            property $ \ a (Positive b) -> evaluate (modExp_lr a b 0 5) `shouldThrow` anyArithException
        it "returns the modular exponentiation" $ do
            property $ prop_exM

-- Ex 3
{- TimeSpent
    Matthisk : 3h
-}
composites :: [Integer]
composites = composites' [2..] primes where
	composites' (x:xs) primes@(p:ps)
		| x < p = x : composites' xs primes
		| otherwise = composites' xs ps

prop_composite :: Integer -> Bool
prop_composite x = or $ map (\ y -> rem x y == 0) [2..(intSqrt x)] where
	intSqrt :: Integer -> Integer
	intSqrt = round . sqrt . fromIntegral

prop_sorted :: (Ord a) => [a] -> Bool
prop_sorted xs = all (\(x, y) -> x <= y) $ zip xs (tail xs)

prop_no_dups :: (Eq a) => [a] -> Bool
prop_no_dups xs = (length xs) == (length $ nub xs)

{- Hspec specification to test the composites array
    (testing the first 1000 composites)
-}
test_composites :: IO ()
test_composites = hspec $ do
    describe "composites" $ do
        it "returns a list of composite numbers" $ do
            and $ take 1000 $ map prop_composite composites
        it "returns an ordered list" $ do
            prop_sorted $ take 1000 $ composites
        it "contains no duplicates" $ do
            prop_no_dups $ take 1000 $ composites

-- Ex 4
{- TimeSpent
    Matthisk : 45m
-}
test_primality_check :: (Int -> Integer -> IO (Bool)) -> Int -> Int -> [Integer] -> IO ([Integer])
test_primality_check check k n list = do
	let numbers = take n list
	vs <- sequence $ map (check k) $ numbers
	let results = filter (/=0) $ zipWith (\ x y -> if x then y else 0) vs numbers
	return $ results

stats_primality_check :: (Int -> Int -> IO ([Integer])) -> Int -> Int -> Int -> IO (Int)
stats_primality_check _ 0 _ _ = return $ 0
stats_primality_check f t k n = do
	r <- f k n
	rest <- stats_primality_check f (t-1) k n
	return $ length r + rest

{- This function lets you test the fermat primality check you supply it with:
    list - the list of numbers to check the primality check on, this should be either composites or the carmichael numbers
    k    - the number of tries for the primality check (e.g. 5)
    n    - the number of items to check from the list

    returns - The list of numbers that are probably prime according to the primality check

    this function can show how many elements are marked probable prime by the primality check without actually being prime  
-}
test_primeF list k n = test_primality_check primeF k n list

{- This function runs the previous function multiple times and returns the average amount of marked probable primes from the input list
    t - times to run test_primeF
    k - the number of tries for the primality check (e.g. 5)
    n - number of items to check from the supplied list
    list - the list of numbers to check the primality check on, this should be either composites or the carmichael numbers

    returns - the average amount of probable primes found by the primality check

    Using this function you can research how many false positives the primality check returns on average
-}
stats_primeF t k n list = do
	stats <- stats_primality_check (test_primeF list) t k n
	return $ div stats t

-- Ex 5
{- TimeSpent
    Matthisk : 45m
-}
carmichael :: [Integer]
carmichael = [ (6*k+1)*(12*k+1)*(18*k+1) |
	k <- [2..],
	isPrime (6*k+1),
	isPrime (12*k+1),
	isPrime (18*k+1) ]

{-
    Using this function you can see why the carmichael numbers will so often return probable prime with the primeF primality check
    if you call /littleFermat $ carmichael !! x/ for any x, it will calculate all the checks (not just random ones). As you can see
    most of these checks are 1, thus the primeF check will very often return probable prime, while the carmichael numbers are actually composites
-}
littleFermat n = map (\ x -> exM x (n-1) n) [1..(n-1)]
{-
    This is the primality check for all (x < n), this is a slow implementation to check if a number is prime
-}
testLittleFermat = all (==1) . littleFermat

-- Ex 6
{- TimeSpent
    Matthisk : 10m
-}
{-
    These two functions are the same as test_primeF and stats_primeF but using the Miller Rabin primality check instead of little fermats theorem

    the amount of false positives when calling with composites list or carmichael numbers is significantly smaller then using primeF
-}
test_primeMR list k n = test_primality_check primeMR k n list
stats_primeMR t k n list = do
	stats <- stats_primality_check (test_primeMR list) t k n
	return $ div stats t

-- Ex 7
{- TimeSpent
    Matthisk : 30m
-}
mersennes :: [Integer]
mersennes = [m1, m2, m3, m5, m6, m7, m8, m9, m10, m11, m12, m13, m14, m15, m16, m17, m18, m19, m20, m21, m22, m23, m24, m25]

test_mersennes k n = sequence $ map (primeMR k) (take n mersennes)

-- Defines (2^p - 1) probable prime with a probability of 4^-5
find_mersenne :: Integer -> IO (Maybe (Integer))
find_mersenne p
    | isPrime p = do
        let p' = (2^p - 1)
        prime <- primeMR 5 p'
        if prime
            then return $ Just p'
            else return $ Nothing
    | otherwise = return $ Nothing

-- Ex 8
{- TimeSpent
    Matthisk : 2h
-}
randomLargeNumber :: Int -> IO (Integer)
randomLargeNumber n 
    | n < 1 = error "Negative exponent"
    | otherwise = do
    	let bounds = (2^(n-1),2^n-1)
    	randomRIO bounds

-- Generates random large numbers (with bit size n) if it is even it adds one otherwise it returns immediately
randomLargeOddNumber :: Int -> IO (Integer)
randomLargeOddNumber n = do
	p <- randomLargeNumber n
	if odd p
		then return p
		else return (p+1)

-- Using 50 tries the primality test defines n probably prime with a probability of 4^-50 (or 2^-100) which is an acceptable error bound 
randomLargePrime :: Int -> IO (Integer)
randomLargePrime n = do
	p <- randomLargeOddNumber n
	isP <- primeMR 50 p
	if isP 
		then return p
		else randomLargePrime n

randomPrimeTuple :: Int -> IO (Integer,Integer)
randomPrimeTuple n = do
	p <- randomLargePrime n
	q <- randomLargePrime n
	return $ (p,q)

{-
    This function generates a RSA public private key pair, it uses prime numbers with bitlength 1024
    The encryption key is 2048 bits long.
-}
randomRSA :: IO ((Integer,Integer),(Integer,Integer))
randomRSA = do
	(p,q) <- randomPrimeTuple 1024
	let (pub,pri) = (rsa_public p q,rsa_private p q)
	print "Public Key:"
	print "==========================================="
	print $ pub
	print "Private Key:"
	print "==========================================="
	print $ pri
	return $ (pub,pri)

prop_bit_size :: Integer -> Int -> Bool
prop_bit_size 1 1 = True
prop_bit_size x n = (ceiling $ logBase 2 $ fromInteger x) == n

test_rln = hspec $ do
    describe "randomLargeNumber" $ do
        it "throws an error for n < 1" $ do
            property $ forAll (choose (-1024,0)) $ \ n -> randomLargeNumber n `shouldThrow` errorCall "Negative exponent"
        it "returns a number with bit length n" $ do
            property $ forAll (choose (1,1024)) $ \ n -> randomLargeNumber n >>= \ x -> prop_bit_size x n `shouldBe` True

test_rlon = hspec $ do
    describe "randomLargeOddNumber" $ do
        it "throws an error for n < 1" $ do
            property $ forAll (choose (-1024,0)) $ \ n -> randomLargeOddNumber n `shouldThrow` errorCall "Negative exponent" 
        it "returns an odd number" $ do
            property $ forAll (choose (1, 1024)) $ \ n -> randomLargeOddNumber n >>= \ x -> odd x `shouldBe` True

test_rpn = hspec $ do
    describe "randomLargePrime" $ do
        it "throws an error for n < 1" $ do
            property $ forAll (choose (-1024,0)) $ \ n -> randomLargePrime n `shouldThrow` errorCall "Negative exponent"
        it "returns a prime number" $ do
            property $ forAll (choose (1, 101)) $ \ n -> randomLargePrime n >>= \ x -> primeMR 50 x `shouldReturn` True

test_all = test_modExp >> test_composites >> test_rln >> test_rlon >> test_rpn

