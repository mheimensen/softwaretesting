module Bonus where
import Lab1Bonus

-- BONUS

-- Excercise 1
myLength :: [a] -> Int
myLength = foldr (\ x y -> 1 + y) 0

myElem :: Eq a => a -> [a] -> Bool
myElem e = foldr (\ x y -> e == x || y) False 

myOr :: [Bool] -> Bool
myOr = foldr (||) False

myMap :: (a -> b) -> [a] -> [b]
myMap f = foldr (\ x y -> f x : y) [] 

myFilter :: (a -> Bool) -> [a] -> [a]
myFilter p = foldr (\ x y -> if p x then y else x : y) []

(+++) :: [a] -> [a] -> [a]
(+++) xs ys = foldr (\ x y -> x : y) ys xs

myReverse :: [a] -> [a]
myReverse = foldr (\ x y -> y ++ [x]) []

-- Excercise 2
myReverse' :: [a] -> [a]
myReverse' = foldl (\ x y -> y : x) []

-- Excercise 3
-- Foldr because it has a starting point foldl first has to construct functions for the infinite list

-- Excercise 4
sign1', sign2' :: (Creature,Creature) -> Bool
sign1' (x,y) = x == Lady || y == Lady
sign2' (x,y) = x == Tiger

solution2 :: [(Creature,Creature)]
solution2 = [ (x,y) | x <- [Lady,Tiger],
					  y <- [Lady,Tiger],
					  sign1' (x,y) == sign2' (x,y)]

-- Excercise 5
john', bill' :: (Islander,Islander) -> Bool
john' (x,y) = x == y
bill' (x,y) = x /= y

solution4 :: [(Islander,Islander)]
solution4 = [ (x,y) | x <- [Knight,Knave],
                      y <- [Knight,Knave],
                      john' (x,y) == (x == Knight) && bill' (x,y) == (y == Knight)]

-- Excercise 7
nmTruths :: Int -> [Bool] -> Bool
nmTruths x = (==x) . length . filter (\y -> y == True)

solution :: [Boy]
solution = [ x | x <- boys, 
				 nmTruths 3 (map (\f -> f x) declarations)]

honest :: [Boy]
honest = [ b | (f, b) <- table,
			   and (map f solution)]