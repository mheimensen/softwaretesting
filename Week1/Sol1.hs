module Sol1 where
import GS

-- Exercise 1.09
mxmInt :: [Int] -> Int
mxmInt [] = error "empty list"
mxmInt [x] = x
mxmInt (x:xs) = max x ( mxmInt xs )

-- Exercise 1.10
removeFst :: Int -> [Int] -> [Int]
removeFst _ [] = []
removeFst x (y:ys) = 
	if x == y 
	then ys 
	else y : removeFst x ys

-- Excercise 1.13
count :: Char -> String -> Int
count _ [] = 0
count c (x:xs) = 
	if c == x 
	then 1 + count c xs 
	else count c xs

-- Excercise 1.14
blowup :: String -> String
blowup s = blowup' 1 s where
	blowup' :: Int -> String -> String
	blowup' _ [] = []
	blowup' x (y:ys) = charmul x y ++ blowup' (x+1) ys

charmul :: Int -> Char -> String
charmul 0 _ = []
charmul x c = c : charmul (x-1) c

-- Excercise 1.15
removeFst' :: Eq a => a -> [a] -> [a]
removeFst' _ [] = []
removeFst' x (y:ys) =
	if x == y
	then ys
	else y : removeFst' x ys

sort :: Ord a => [a] -> [a]
sort [] = []
sort xs = m : (sort (removeFst' m xs)) where m = minimum xs

-- Excercise 1.17
substr :: String -> String -> Bool
substr [] _ = True
substr xs [] = False
substr xs (y:ys) = if prefix xs (y:ys) then True else substr xs ys

-- Excercise 1.20
lengths = map length

-- Exercise 1.21
sumLengths xs = sum (lengths xs)