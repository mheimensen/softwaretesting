module Sol2 where
import TAMO

-- Excercise 2.12
th1 = not True == False <=> not False == True 
th2 = lequiv (\ p -> not p) (\ p -> p ==> False) 
th3a = lequiv (\ p -> p || True) (\ p -> True)
th3b = lequiv (\ p -> p && False) (\ p -> False)
th4a = lequiv (\ p -> p || False) id
th4b = lequiv (\ p -> p && True) id
th5 = lequiv (\ p -> p || not p) (\ p -> True)
th6 = lequiv (\ p -> p && not p) (\ p -> False)

-- Excercise 2.15
uvalid1 :: (Bool -> Bool) -> Bool
uvalid1 bf = not (valid1 bf)

uvalid2 :: (Bool -> Bool -> Bool) -> Bool
uvalid2 bf = not (valid2 bf)

uvalid3 :: (Bool -> Bool -> Bool -> Bool) -> Bool
uvalid3 bf = not (valid3 bf)

-- Excercise 2.20
ch1 = lequiv (\ p q -> (not p) ==> q) (\ p q -> p ==> (not q))
ch2 = lequiv (\ p q -> (not p) ==> q) (\ p q -> q ==> (not p))
ch3 = lequiv (\ p q -> (not p) ==> q) (\ p q -> (not q) ==> p)
ch4 = lequiv (\ p q r -> p ==> ( q ==> r )) (\ p q r -> q ==> (p ==> r))
ch5 = lequiv (\ p q r -> p ==> ( q ==> r )) (\ p q r -> (p ==> q) ==> r)
ch6 = lequiv (\ p q -> (p ==> q) ==> p) (\ p q -> p)
ch7 = lequiv (\ p q r -> p || q ==> r) (\ p q r -> ( p ==> r ) && ( q ==> r ))

-- Excercise 2.51
unique :: (a -> Bool) -> [a] -> Bool
unique p = xor . map p

xor :: [Bool] -> Bool
xor [x] = x
xor (x:xs) = x <+> (xor xs)

-- Excercise 2.52
parity :: [Bool] -> Bool
parity xs = rem ((foldl (+) 0 . map (\ x -> if x then 1 else 0)) xs) 2 == 0

-- Exercise 2.53
evenNR :: (a -> Bool) -> [a] -> Bool
evenNR p = parity . map p
